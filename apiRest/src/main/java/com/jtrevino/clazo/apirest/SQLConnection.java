/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jtrevino.clazo.apirest;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author cristianlazo
 */
public class SQLConnection {
    
    String user = "horariosupv";
    String password = "by6V7#ZJ";
    String url = "jdbc:mysql://localhost:3306/horarios?user=" + user + "&password=" + password;
    
    // JDBC variables for opening and managing connection 
    private static Connection con;
    private static Statement stmt;
    private static ResultSet rs;

    
    public SQLConnection() {
    }
    
    public Connection Connection() {
        try {
            // opening database connection to MySQL server
            con = DriverManager.getConnection(this.url, this.user, this.password);
            if (con != null) {
                //System.out.println("Conectado");
            }
            
        } catch (SQLException e) {
            //System.out.println("No se pudo conectar a la base de datos");
            e.printStackTrace();
        }
        return con;
    }
    
    
    /*************************************
     * ESTOS METODOS SON DE PRUEBA PARA EL MANEJO DE LOS QUERYS
    *************************************/
    /*public JsonArray selectQuery() throws SQLException {
        String query = "SELECT idcarrera, nombre_carrera FROM carrera";
        JsonArray respuesta = new JsonArray();
        
        try {
            // opening database connection to MySQL server
            con = DriverManager.getConnection(this.url, this.user, this.password);
            
            if (con != null) {
                // getting Statement object to execute query
                stmt = con.createStatement();

                rs = stmt.executeQuery(query); 
                
                while (rs.next()) {
                    JsonObject objeto = new JsonObject();
                    String id = rs.getString(1); 
                    String nombre = rs.getString(2); 
                    
                    objeto.addProperty("idcarrera", id);
                    objeto.addProperty("nombre_carrera", nombre);
                    
                    // Añadir objeto al la lista JSON
                    respuesta.add(objeto);
                }
                   
            }
            
        } catch (SQLException e) {
            System.out.println("No se pudo conectar a la base de datos");
            e.printStackTrace();
        }
        return respuesta;
       
    }
    
    public JsonArray selectQueryId(String idcarrera) throws SQLException {
        String query = "SELECT idcarrera, nombre_carrera FROM carrera WHERE idcarrera = "+idcarrera;
        JsonArray respuesta = new JsonArray();
        
        try {
            // opening database connection to MySQL server
            con = DriverManager.getConnection(this.url, this.user, this.password);
            
            if (con != null) {
                // getting Statement object to execute query
                stmt = con.createStatement();

                rs = stmt.executeQuery(query); 
                
                while (rs.next()) {
                    JsonObject objeto = new JsonObject();
                    String id = rs.getString(1); 
                    String nombre = rs.getString(2); 
                    
                    objeto.addProperty("idcarrera", id);
                    objeto.addProperty("nombre_carrera", nombre);
                    
                    // Añadir objeto al la lista JSON
                    respuesta.add(objeto);
                }
                   
            }
            
        } catch (SQLException e) {
            System.out.println("No se pudo conectar a la base de datos");
            e.printStackTrace();
        }
        return respuesta;
    }
    
    public void insertQuery(String nombre_carrera) throws SQLException {
        String query = "INSERT INTO carrera (nombre_carrera) VALUES ('"+nombre_carrera+"')";
        
        try {
            // opening database connection to MySQL server
            con = DriverManager.getConnection(this.url, this.user, this.password);
            
            if (con != null) {
                // getting Statement object to execute query
                stmt = con.createStatement();

                stmt.executeUpdate(query);
            }
            
        } catch (SQLException e) {
            System.out.println("No se pudo conectar a la base de datos");
            e.printStackTrace();
        }
    }
    
    public void updateQuery(String id, String nombre_carrera) throws SQLException {
        String query = "UPDATE carrera SET nombre_carrera = '"+nombre_carrera+"' WHERE idcarrera = "+id;
        
        try {
            // opening database connection to MySQL server
            con = DriverManager.getConnection(this.url, this.user, this.password);
            
            if (con != null) {
                // getting Statement object to execute query
                stmt = con.createStatement();

                stmt.executeUpdate(query);
            }
            
        } catch (SQLException e) {
            System.out.println("No se pudo conectar a la base de datos");
            e.printStackTrace();
        }
    }*/

    
}
