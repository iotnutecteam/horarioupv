/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jtrevino.clazo.apirest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import java.util.HashMap;
import java.util.Map;
import spark.ModelAndView;
import spark.Spark;
import static spark.Spark.*;
import spark.template.jtwig.JtwigTemplateEngine;
import controllers.*;
import spark.QueryParamsMap;

/**
 *
 * @author cristianlazo
 */
public class main {
    private static final String SESSION_NAME = "username";

    
    public static void main(String[] args) throws Exception {
        
        //Instancias a las clases
        Menu menu = new Menu();
        Materias materias = new Materias();
        Carreras carreras = new Carreras();
        PlanEstudios planes = new PlanEstudios();
        Disponibilidad disponibilidad = new Disponibilidad();
        Maestros maestros = new Maestros();
        Grupos grupos = new Grupos();
        PuntosConfianza puntos = new PuntosConfianza();
        Aulas aulas = new Aulas();
        Horarios horarios = new Horarios();
        
        //Conexion a SQL
        final SQLConnection sql = new SQLConnection();
        
        //Guardar el JSON
        String json;
        
        //
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        
        // Configurar directorio de estilos
        Spark.staticFileLocation("/static");
        
        //Pasar valores a la vista
        Map map = new HashMap();
        //map.put("menu", "sdfsdfdssdfsdd");
        //map.put("name", "Lazo");
       
        
        //Ruta para realizar login las materias
        get("/login", (request, response) -> {
            String usuario = request.queryParams("usuario");
            String password = request.queryParams("password");
            int accseso = 0;
            
            //Verficiar que no este vacio los campos de usuario y contaseña
            if (!"".equals(usuario) && !"".equals(password)) {
                Login login = new Login(usuario, password);
                accseso = login.checkLogin();
                
                //Si el incio de sesion es correcto, se activa la session
                if (accseso == 1) {
                    request.session().attribute(SESSION_NAME, SharedData.getClave());
                    String session = request.session().attribute(SESSION_NAME);
                    map.put("session", session);
                    map.put("rol",SharedData.getTipoUsuario());
                }
            }
            
            return accseso;
        });
        
        get("/menu", (request, response) -> {
            
            JsonArray respuesta = new JsonArray();
            // Obtener menu dinamico
            respuesta = menu.crearMenu();
            // Transformar salida a JSON
            String jsonOutput = gson.toJson(respuesta);
            
            return jsonOutput;
        });
        
        //Aqui se haran todas las oepraciones necesarias para la ruta de home
        get("/homeConfig", (request, response) -> {
            JsonArray respuesta = new JsonArray();
            String jsonOutput = "";
                        
            jsonOutput = gson.toJson(respuesta);
               
            return jsonOutput;
        });
        
        
        /******************************
         * RUTAS PARA MODULO "MATERIAS"
        ******************************/
        
        //Recuperar los planes de estudio
        get("/materias/PlanEstudio", (request, response) -> {
            JsonArray respuesta = new JsonArray();
            
            respuesta = materias.obtenerPlanEstudios();
            // Transformar salida a JSON
            String jsonOutput = gson.toJson(respuesta);
                        
            return jsonOutput;
        });
        
        //Ruta para recuperar las materias
        get("/materias/materiasPlan", (request, response) -> {
            String clv_plan = request.queryParams("clv_plan");
            JsonArray respuesta = new JsonArray();
            
            respuesta = materias.obtenerMaterias(clv_plan);
            // Transformar salida a JSON
            String jsonOutput = gson.toJson(respuesta);
            
            map.put("tipo", SharedData.getTipoUsuario());
                        
            return jsonOutput;
        });
        
        //Ruta para obtener datos de la materia
         get("/materia/datos", (req,res)-> {
            String id = req.queryParams("id");
            
            JsonArray respuesta = new JsonArray();
            
            respuesta = materias.obtenerDatosMateria(id);
            // Transformar salida a JSON
            String jsonOutput = gson.toJson(respuesta);
            
           return jsonOutput; 
        });
        
        //Ruta para editar materia
         get("/materia/editar", (req,res)-> {
            String id = req.queryParams("id");
            String clave = req.queryParams("clave");
            String nombreMateria = req.queryParams("nombreMateria");
            String creditos = req.queryParams("creditos");
            String tipoMateria = req.queryParams("tipoMateria");
            String numeroHoras = req.queryParams("numeroHoras");
            
            materias.actualizarMateria(id, clave, nombreMateria, creditos, tipoMateria, numeroHoras);
            
            
           return 0; 
        });
           
        /******************************
         * RUTAS PARA MODULO "CARRERAS"
        ******************************/
        //Recuperar las carreras
        get("/carreras/getCarreras", (request, response) -> {
            JsonArray respuesta = new JsonArray();
            
            respuesta = carreras.obtenerCarreras();
            // Transformar salida a JSON
            String jsonOutput = gson.toJson(respuesta);
                        
            return jsonOutput;
        });
        
        
        //Añadir o editar una carrera
        get("/carreras/nuevaCarrera", (request, response) -> {
            String id = request.queryParams("id");
            String nombreCarrera = request.queryParams("nombre");
            
            //Se agrega una nueva carrera
            if (id == null || "".equals(id)) {
                carreras.insertarCarrera(nombreCarrera);
            //Se esta editando un carera
            } else{
                carreras.actualizarCarrer(id, nombreCarrera);
            }
                        
            return 0;
        });
        
        
        /******************************
         * RUTAS PARA MODULO "PLAN DE ESTUDIOS"
        ******************************/
        
        //Recuperar los planes de estudio
        get("/planEstudios/getPlanes", (request, response) -> {
            JsonArray respuesta = new JsonArray();
            
            respuesta = planes.obtenerPlanes();
            // Transformar salida a JSON
            String jsonOutput = gson.toJson(respuesta);
                        
            return jsonOutput;
        });
        
        //Añadir o editar un plan
        get("/planEstudios/nuevoPlan", (request, response) -> {
            String oldClavePlan = request.queryParams("oldClavePlan");
            String clavePlan = request.queryParams("clavePlan");
            String nombrePlan = request.queryParams("nombrePlan");
            String nivelPlan = request.queryParams("nivelPlan");
            String carreraPlan = request.queryParams("carreraPlan");
            
            //Se agrega un nuevo plan
            if (oldClavePlan == null || "".equals(oldClavePlan)) {
                planes.insertarPlan(clavePlan, nombrePlan, nivelPlan, carreraPlan);
            //Se esta editando un plan
            } else{
                planes.actualizarPlan(oldClavePlan, clavePlan, nombrePlan, nivelPlan, carreraPlan);
            }
                        
            return 0;
        });
        
        /******************************
         * RUTAS PARA MODULO "MAESTROS"
        ******************************/
        //Recuperar los maestros
        get("/maestros/getMaestros", (request, response) -> {
            JsonArray respuesta = new JsonArray();
            
            respuesta = maestros.obtenerMaestros();
            // Transformar salida a JSON
            String jsonOutput = gson.toJson(respuesta);
                        
            return jsonOutput;
        });
        
        //Recuperar los datos de algun maestro en especifico
        get("/maestros/ObtenerRegistroMaestro", (request, response) -> {
            String claveUsuario = request.queryParams("claveUsuario");
                        
            JsonArray respuesta = new JsonArray();
            
            respuesta = maestros.obtenerRegistroMaestro(claveUsuario);
            // Transformar salida a JSON
            String jsonOutput = gson.toJson(respuesta);
                                    
            return jsonOutput;
        });
        
        //Añadir o editar un maestro
        get("/maestros/nuevoMaestro", (request, response) -> {
            String claveUsuario = request.queryParams("claveUsuario");
            String usuario = request.queryParams("usuario");
            String password = request.queryParams("password");
            String nombreCompleto = request.queryParams("nombreCompleto");
            String telefono = request.queryParams("telefono");
            String nivelAcademico = request.queryParams("nivelAcademico");
            String carrera = request.queryParams("carrera");
            String contrato = request.queryParams("contrato");
            String imr = request.queryParams("imr");
            
            //Se agrega un nuevo plan
            if (claveUsuario == null || "".equals(claveUsuario)) {
                maestros.insertarMaestro(usuario, password, nombreCompleto, telefono, nivelAcademico, carrera, contrato, imr);
            //Se esta editando un plan
            } else{
                maestros.actualizarMaestro(claveUsuario, password, nombreCompleto, telefono, nivelAcademico, carrera, contrato, imr);
            }
                        
            return 0;
        });
        
        //Eliminar un registro de la tabla usuarios
        get("/maestros/eliminarRegistro", (request, response) -> {
            String claveUsuario = request.queryParams("claveUsuario");
            
            maestros.eliminarRegistroMaestro(claveUsuario);
                                    
            return 0;
        });
        

        //Recuperar los maestros
        /*get("/maestros/obtenerCompartir", (request, response) -> {
            JsonArray respuesta = new JsonArray();
            
            respuesta = maestros.obtenerCompartir("lol");
            // Transformar salida a JSON
            String jsonOutput = gson.toJson(respuesta);
                        
            return jsonOutput;
        });*/
        
        
        /******************************
         * RUTAS PARA MODULO "GRUPOS"
        ******************************/
        
        //Recuperar los grupos
        get("/grupos/getGrupos", (request, response) -> {
            JsonArray respuesta = new JsonArray();
            
            respuesta = grupos.obtenerGrupos();
            // Transformar salida a JSON
            String jsonOutput = gson.toJson(respuesta);
                        
            return jsonOutput;
        });
        
        
        //Recuperar el horario del grupo
        get("/grupos/getHorario", (request, response) -> {
            String turno = request.queryParams("turno");
            String grupo = request.queryParams("grupo");
            
            JsonArray respuesta = new JsonArray();
            
            respuesta = grupos.obtenerHorario(turno, grupo);
            // Transformar salida a JSON
            String jsonOutput = gson.toJson(respuesta);
                        
            return jsonOutput;
        });
        
        //Recuperar plan de estudios para crear un nuevo grupo
        get("/grupos/getPlanEstudios", (request, response) -> {
            JsonArray respuesta = new JsonArray();
            
            respuesta = grupos.obtenerPlanEstudios();
            // Transformar salida a JSON
            String jsonOutput = gson.toJson(respuesta);
                        
            return jsonOutput;
        });
        
        //Añadir un nuevo grupo
        get("/grupos/nuevoGrupo", (request, response) -> {
            String clave = request.queryParams("clave");
            String cuatrimestre = request.queryParams("cuatrimestre");
            String planEstudio = request.queryParams("planEstudio");
            
            grupos.insertarGrupo(clave, cuatrimestre, planEstudio);
                        
            return 0;
        });
        
        
        /******************************
         * RUTAS PARA MODULO "PUNTOS DE CONFIANZa"
        ******************************/
        
        //Recuperar el plan de estudios
        get("/puntosConfianza/getPlan", (request, response) -> {
            JsonArray respuesta = new JsonArray();
            
            respuesta = puntos.obtenerPlan();
            // Transformar salida a JSON
            String jsonOutput = gson.toJson(respuesta);
                        
            return jsonOutput;
        });
        
        //Recuperar los profesores
        get("/puntosConfianza/getProfesores", (request, response) -> {
            JsonArray respuesta = new JsonArray();
            
            respuesta = puntos.obtenerProfesores();
            // Transformar salida a JSON
            String jsonOutput = gson.toJson(respuesta);
                        
            return jsonOutput;
        });
        
        
        
        //Recuperar los nombres de materias y sus puntos de confianza
        get("/puntosConfianza/getPuntosMaterias", (request, response) -> {
            JsonArray respuesta = new JsonArray();
            String profesor = request.queryParams("profesor");
            String planEstudio = request.queryParams("planEstudio");
            String rol = request.queryParams("rol");
            
            //Validar si es un director el que hace la peticion
            if ("Director".equals(rol)) {
                System.out.println("Entre en director");
                respuesta = puntos.obtenerPuntosMaterias(profesor, planEstudio);
            } else {
                System.out.println("Entre en el else");
                respuesta = puntos.obtenerPuntosMateriasProf(planEstudio);
            }
            
            // Transformar salida a JSON
            String jsonOutput = gson.toJson(respuesta);
                        
            return jsonOutput;
        });
        
        /******************************
         * RUTAS PARA MODULO "DISPONIBILIDAD"
        ******************************/
        
        // Ruta para obtener listado de profesores
        get("/profesores/listaNombres", (request, response) -> {
            JsonArray respuesta = new JsonArray();
            // Obtener profesores
            respuesta = disponibilidad.obtenerProfesores();
            // Transformar salida a JSON
            String jsonOutput = gson.toJson(respuesta);
                        
            return jsonOutput;
        });
        
        // Ruta para obtener listado de profesores
        get("/disponibilidad/profesor", (request, response) -> {
            JsonArray respuesta = new JsonArray();
            // Capturar parametros de la peticion
            String clv_usuario = request.queryParams("clv_usuario");
            String turno = request.queryParams("turno");
            // Obtener tabla de disponibilidad
            respuesta = disponibilidad.obtenerDisponibilidad(clv_usuario,turno);
            // Transformar salida a JSON
            String jsonOutput = gson.toJson(respuesta);
                        
            return jsonOutput;
        });
        
        // Ruta para actualizar la disponibilidad de un profesor
        get("/disponibilidad/actualizar", (request, response) -> {
            JsonArray respuesta = new JsonArray();
            // Capturar parametros de la peticion
            String clv_usuario = request.queryParams("clv_usuario");
            String dia = request.queryParams("dia");
            String espacio_tiempo = request.queryParams("espacio_tiempo");
            String accion = request.queryParams("accion");
            
            // Obtener tabla de disponibilidad
            respuesta = disponibilidad.actualizarDisponibilidad(clv_usuario,dia,espacio_tiempo,accion);
            // Transformar salida a JSON
            String jsonOutput = gson.toJson(respuesta);
                        
            return jsonOutput;
        });
        
        /******************************
         * RUTAS PARA MODULO "AULAS"
        ******************************/
        
        // Ruta para obtener listado aulas
        get("/aulas/getAulas", (request, response) -> {
            JsonArray respuesta = new JsonArray();
            // Obtener profesores
            respuesta = aulas.obtenerAulas();
            // Transformar salida a JSON
            String jsonOutput = gson.toJson(respuesta);
                        
            return jsonOutput;
        });
        
        // Ruta para obtener listado de categorias
        get("/aulas/getCategorias", (request, response) -> {
            JsonArray respuesta = new JsonArray();
            // Obtener profesores
            respuesta = aulas.obtenerCategorias();
            // Transformar salida a JSON
            String jsonOutput = gson.toJson(respuesta);
                        
            return jsonOutput;
        });
        
        
        // Ruta para agreagar una nueva aula
        get("/aulas/nuevaAula", (request, response) -> {
            String id = request.queryParams("id");
            String clave = request.queryParams("clave");
            String nombreAula = request.queryParams("nombreAula");
            String tipo = request.queryParams("tipo");
            String capacidad = request.queryParams("capacidad");
            
            
            if (id == null || "".equals(id)) {
                aulas.insertarAula(clave, nombreAula, tipo, capacidad);
            } else {
                aulas.actualizarAula(id, clave, nombreAula, tipo, capacidad);
            }
    
            return 0;
        });
        
        // Ruta para agregar nuevos equipos
        get("/aulas/nuevoEquipo", (request, response) -> {
            
            String nombreEquipo = request.queryParams("nombreEquipo");
            String descripcionEquipo = request.queryParams("descripcionEquipo");
            String categoria = request.queryParams("categoria");
            
            aulas.insertarEquipo(nombreEquipo, descripcionEquipo, categoria);
                        
            return 0;
        });
        
        
        // Ruta para agregar nueva categoria
        get("/aulas/nuevaCategoria", (request, response) -> {
            String nombreCategoria = request.queryParams("nombreCategoria");
            String descripcionCategoria = request.queryParams("descripcionCategoria");
 
            aulas.insertarCategoria(nombreCategoria, descripcionCategoria);
                        
            return 0;
        });
        
        
        //Recuperar los datos de algun maestro en especifico
        get("/aulas/obtenerRegistroAula", (request, response) -> {
            String id = request.queryParams("id");
                        
            JsonArray respuesta = new JsonArray();
            
            respuesta = aulas.obtenerRegistroAula(id);
            // Transformar salida a JSON
            String jsonOutput = gson.toJson(respuesta);
                                    
            return jsonOutput;
        });
        
        
        //Recuperar los datos de algun maestro en especifico
        get("/aulas/actualizarAula", (request, response) -> {
            String id = request.queryParams("id");
                        
            JsonArray respuesta = new JsonArray();
            
            respuesta = aulas.obtenerRegistroAula(id);
            // Transformar salida a JSON
            String jsonOutput = gson.toJson(respuesta);
                                    
            return jsonOutput;
        });
        
        /******************************
         * RUTAS PARA MODULO "HORARIOS"
        ******************************/
        
        // Ruta para obtener listado de profesores
        get("/horarios/materiasGrupo", (request, response) -> {
            JsonArray respuesta = new JsonArray();
            // Obtener valor parámetro grupo desde la petición
            String clv_grupo = request.queryParams("clv_grupo");
            // Obtener profesores
            respuesta = horarios.obtenerMateriasGrupo(clv_grupo);
            // Transformar salida a JSON
            String jsonOutput = gson.toJson(respuesta);
            
            return jsonOutput;
        });
        
        // Ruta para obtener el horario de un grupo
        get("/horarios/horarioGrupo", (request, response) -> {
            JsonArray respuesta = new JsonArray();
            // Obtener valor parámetro grupo desde la petición
            String clv_grupo = request.queryParams("clv_grupo");
            String turno = request.queryParams("turno");
            // Obtener profesores
            respuesta = horarios.obtenerHorarioGrupo(clv_grupo,turno);
            // Transformar salida a JSON
            String jsonOutput = gson.toJson(respuesta);
            
            return jsonOutput;
        });
        
        // Ruta para obtener el horario de un grupo
        get("/horarios/actualizar", (request, response) -> {
            JsonArray respuesta = new JsonArray();
            // Obtener valor parámetro grupo desde la petición
            String clv_grupo = request.queryParams("clv_grupo");
            String clv_materia = request.queryParams("clv_materia");
            String clv_profesor = request.queryParams("clv_profesor");
            String nombre_profesor = request.queryParams("nombre_profesor");
            String dia = request.queryParams("dia");
            String hora = request.queryParams("hora");
            
            // Obtener profesores
            respuesta = horarios.actualizarHorarioGrupo(clv_grupo,clv_materia,clv_profesor,nombre_profesor,dia,hora);
            // Transformar salida a JSON
            String jsonOutput = gson.toJson(respuesta);
            
            return jsonOutput;
        });
        
        // Ruta para obtener la tabla de horas semana
        get("/horarios/horasSemana", (request, response) -> {
            JsonArray respuesta = new JsonArray();
            // Obtener valor parámetro grupo desde la petición
            String clv_profesor = request.queryParams("clv_profesor");
            String nombre_profesor = request.queryParams("nombre_profesor");
            
            // Obtener profesores
            respuesta = horarios.obtenerHorasSemana(clv_profesor,nombre_profesor);
            // Transformar salida a JSON
            String jsonOutput = gson.toJson(respuesta);
            
            return jsonOutput;
        });
        
        // Ruta para obtener la tabla de horas semana
        get("/horarios/obtenerIMR", (request, response) -> {
            JsonArray respuesta = new JsonArray();
            // Obtener valor parámetro grupo desde la petición
            String clv_grupo = request.queryParams("clv_grupo");
            
            // Obtener profesores
            respuesta = horarios.obtenerIMRGrupo(clv_grupo);
            // Transformar salida a JSON
            String jsonOutput = gson.toJson(respuesta);
            
            return jsonOutput;
        });
        

        /*//Ruta para obtener usuarios
        get("/carrera", (request, response) -> {
            //Lista para guardar el query
            JsonArray respuesta = new JsonArray();
            // Obtener datos
            respuesta = sql.selectQuery();
            // Transformar salida a JSON
            String jsonOutput = gson.toJson(respuesta);
            return jsonOutput;
        });
         
        //Ruta para obtener usuarios por id
        get("/carrera/:id", (req,res)->{
            String jsonOutput = "";
            
            //Validar que el id sea correcto
            if (req.params(":id") != null) {
                //Lista para guardar el query
                JsonArray respuesta = new JsonArray();
                // Obtener datos
                respuesta = sql.selectQueryId(req.params(":id"));
                // Transformar salida a JSON
                jsonOutput = gson.toJson(respuesta);
            }
            
            return jsonOutput;
        });
        
        //Ruta para insertar un usuario
        post("/carrera/add", (request, response) -> {
            String nombre = request.queryParams("nombre_carrera");
            
            //Validar todos los parametros para que no se escape ningun null
            if (nombre != null){
                sql.insertQuery(nombre);
            }
            return 0;
        });
        
        
        //curl -X PUT -d "nombre_carrera=PATONUÑO" http://localhost:4567/carrera/6
        //Ruta para editar un usuario
        put("/carrera/:id", (request, response) -> {
            String id = request.params(":id");
            String nombre = request.queryParams("nombre_carrera");
            
            //Validar todos los parametros para que no se escape ningun null
            if (id != null && nombre != null) {
                sql.updateQuery(id,nombre);
            }
            
            return 0;
        });*/ 
        
        //Ruta cerrar la sesion
        get("/logout", (request, response) -> {
            request.session().removeAttribute(SESSION_NAME);
            String session = request.session().attribute(SESSION_NAME);
            map.put("session", session);
            return 0;
        });
       
        get("/", (rq, rs) -> new ModelAndView(map, "login.twig"), new JtwigTemplateEngine());
        get("/home", (rq, rs) -> new ModelAndView(map, "hello.twig"), new JtwigTemplateEngine());
        get("/materias", (rq, rs) -> new ModelAndView(map, "materias.twig"), new JtwigTemplateEngine());
        get("/materias/:clv_materia", (rq, rs) -> new ModelAndView(map, "editarMateria.twig"), new JtwigTemplateEngine());
        get("/carreras", (rq, rs) -> new ModelAndView(map, "carreras.twig"), new JtwigTemplateEngine());
        get("/planEstudios", (rq, rs) -> new ModelAndView(map, "planEstudios.twig"), new JtwigTemplateEngine());
        get("/disponibilidad", (rq, rs) -> new ModelAndView(map, "disponibilidad.twig"), new JtwigTemplateEngine());
        get("/maestros", (rq, rs) -> new ModelAndView(map, "maestros.twig"), new JtwigTemplateEngine());
        get("/grupos", (rq, rs) -> new ModelAndView(map, "grupos.twig"), new JtwigTemplateEngine());
        get("/puntosConfianza", (rq, rs) -> new ModelAndView(map, "puntosConfianza.twig"), new JtwigTemplateEngine());
        get("/aulas", (rq, rs) -> new ModelAndView(map, "aulas.twig"), new JtwigTemplateEngine());
        get("/horarios", (rq, rs) -> new ModelAndView(map, "horarios.twig"), new JtwigTemplateEngine());
    }
}
