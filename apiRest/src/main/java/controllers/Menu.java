/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 *
 * @author cristianlazo
 */
public class Menu {

    public Menu() {
    }
    
    public JsonArray crearMenu() {
        JsonArray respuesta = new JsonArray();
        JsonObject opcion = new JsonObject();
        
        // Obtener el tipo de usuario
        String tipo = SharedData.getTipoUsuario();
        
        //System.out.println("--- TIPO DE USUARIO: "+tipo);
        
        if("Administrador".equals(tipo) || "Director".equals(tipo) || "Profesor".equals(tipo)){
            opcion = new JsonObject();
            opcion.addProperty("nombre", "Disponibilidad");
            opcion.addProperty("url", "/disponibilidad");
            opcion.addProperty("icono", "fas fa-business-time");
            respuesta.add(opcion);
            
            opcion = new JsonObject();
            opcion.addProperty("nombre", "Materias");
            opcion.addProperty("url", "/materias");
            opcion.addProperty("icono", "fas fa-book");
            respuesta.add(opcion);
            
            if("Director".equals(tipo) || "Administrador".equals(tipo)){
                opcion = new JsonObject();
                opcion.addProperty("nombre", "Maestros");
                opcion.addProperty("url", "/maestros");
                opcion.addProperty("icono", "fas fa-chalkboard-teacher");
                respuesta.add(opcion);
            
                opcion = new JsonObject();
                opcion.addProperty("nombre", "Carreras");
                opcion.addProperty("url", "/carreras");
                opcion.addProperty("icono", "fas fa-university");
                respuesta.add(opcion);
            
                opcion = new JsonObject();
                opcion.addProperty("nombre", "Plan de Estudio");
                opcion.addProperty("url", "/planEstudios");
                opcion.addProperty("icono", "fas fa-graduation-cap");
                respuesta.add(opcion);

                opcion = new JsonObject();
                opcion.addProperty("nombre", "Aulas");
                opcion.addProperty("url", "/aulas");
                opcion.addProperty("icono", "fas fa-school");
                respuesta.add(opcion);
            }
            
            opcion = new JsonObject();
            opcion.addProperty("nombre", "Grupos");
            opcion.addProperty("url", "/grupos");
            opcion.addProperty("icono", "fas fa-users");
            respuesta.add(opcion);
            
            opcion = new JsonObject();
            opcion.addProperty("nombre", "Horarios");
            opcion.addProperty("url", "/horarios");
            opcion.addProperty("icono", "far fa-calendar-alt");
            respuesta.add(opcion);
            
            opcion = new JsonObject();
            opcion.addProperty("nombre", "Puntos de Confianza");
            opcion.addProperty("url", "/puntosConfianza");
            opcion.addProperty("icono", "fa fa-star");
            respuesta.add(opcion);
               
        }
            
        return respuesta;
    }
    
}
