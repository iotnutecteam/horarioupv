/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jtrevino.clazo.apirest.SQLConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author cristianlazo
 */
public class Maestros {

    public Maestros() {
    }
    
    public JsonArray obtenerMaestros() throws SQLException {
        JsonArray respuesta = new JsonArray();
        JsonObject opcion = new JsonObject();

        String query = "SELECT nombre_usuario, clv_usuario, telefono, nivel_ads, contrato, nombre_carrera, IMR FROM usuarios NATURAL JOIN carrera WHERE tipo_usuario = 'PROF' AND idcarrera = " + SharedData.getCarrera();
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();

        ResultSet rs = stmt.executeQuery(query); 
        
        while (rs.next()) {
            opcion = new JsonObject();
            opcion.addProperty("nombre_usuario", rs.getString("nombre_usuario"));
            opcion.addProperty("clv_usuario", rs.getString("clv_usuario"));
            opcion.addProperty("telefono", rs.getString("telefono"));
            opcion.addProperty("nivel_ads", rs.getString("nivel_ads"));
            opcion.addProperty("contrato", rs.getString("contrato"));
            opcion.addProperty("nombre_carrera", rs.getString("nombre_carrera"));
            opcion.addProperty("IMR", rs.getString("IMR"));
            respuesta.add(opcion);
        }
        
        return respuesta;
    }
    
    public JsonArray obtenerRegistroMaestro(String claveUsuario) throws SQLException {
        JsonArray respuesta = new JsonArray();
        JsonObject opcion = new JsonObject();

        String query = "SELECT nombre_usuario, pass_usuario, clv_usuario, telefono, nivel_ads, contrato, nombre_carrera, IMR FROM usuarios NATURAL JOIN carrera WHERE clv_usuario = '" +claveUsuario+"'";
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();

        ResultSet rs = stmt.executeQuery(query); 
        
        while (rs.next()) {
            opcion = new JsonObject();
            opcion.addProperty("nombre_usuario", rs.getString("nombre_usuario"));
            opcion.addProperty("pass_usuario", rs.getString("pass_usuario"));
            opcion.addProperty("clv_usuario", rs.getString("clv_usuario"));
            opcion.addProperty("telefono", rs.getString("telefono"));
            opcion.addProperty("nivel_ads", rs.getString("nivel_ads"));
            opcion.addProperty("contrato", rs.getString("contrato"));
            opcion.addProperty("nombre_carrera", rs.getString("nombre_carrera"));
            opcion.addProperty("IMR", rs.getString("IMR"));
            respuesta.add(opcion);
        }
        
        return respuesta;
    }
    
    public void actualizarMaestro(String claveUsuario, String password, String nombreCompleto, String telefono, String nivelAcademico, String carrera, String contrato, String imr) throws SQLException {
        String query = "UPDATE usuarios SET pass_usuario = '"+password+"', nombre_usuario = '"+nombreCompleto+"', telefono = '"+telefono+"', nivel_ads = '"+nivelAcademico+"', idcarrera = (SELECT idcarrera FROM carrera WHERE nombre_carrera = '"+carrera+"'), contrato = '"+contrato+"', IMR = "+imr+" WHERE clv_usuario = '"+claveUsuario+"'";
        
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();

        stmt.executeUpdate(query); 
        
    }
    
    public void insertarMaestro(String usuario, String password, String nombreCompleto, String telefono, String nivelAcademico, String carrera, String contrato, String imr) throws SQLException {
        String query = "INSERT INTO usuarios (clv_usuario, pass_usuario, nombre_usuario, telefono, nivel_ads, idcarrera, contrato, IMR, tipo_usuario) VALUES ('"+usuario+"', '"+password+"', '"+nombreCompleto+"', '"+telefono+"', '"+nivelAcademico+"', (SELECT idcarrera FROM carrera WHERE nombre_carrera = '"+carrera+"'), '"+contrato+"', "+imr+", 'PROF')";
        
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();

        stmt.executeUpdate(query);
        
    }
    
    public void eliminarRegistroMaestro(String claveUsuario) throws SQLException {
        String query = "DELETE FROM usuarios WHERE clv_usuario = '"+claveUsuario+"'";
        
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();

        stmt.executeUpdate(query);
    }
    
    public JsonArray obtenerCompartir(String carrera_origen) throws SQLException {
        JsonArray respuesta = new JsonArray();
        JsonObject opcion = new JsonObject();

        String query = "SELECT idcarrera, nombre_carrera FROM carrera WHERE NOT nombre_carrera ='" + carrera_origen + "'";
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();

        ResultSet rs = stmt.executeQuery(query); 
        
        while (rs.next()) {
            opcion = new JsonObject();
            opcion.addProperty("idcarrera", rs.getString("idcarrera"));
            opcion.addProperty("nombre_carrera", rs.getString("nombre_carrera"));
            respuesta.add(opcion);
        }
        
        return respuesta;
    }
    
}
