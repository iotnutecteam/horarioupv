/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jtrevino.clazo.apirest.SQLConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author cristianlazo
 */
public class Aulas {

    public Aulas() {
    }
    
    public JsonArray obtenerAulas() throws SQLException {
        JsonArray respuesta = new JsonArray();
        JsonObject opcion = new JsonObject();

        String query = "SELECT id, nombre, tipo, capacidad FROM aulas";
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();

        ResultSet rs = stmt.executeQuery(query); 
        
        while (rs.next()) {
            opcion = new JsonObject();
            opcion.addProperty("id", rs.getString("id"));
            opcion.addProperty("nombre", rs.getString("nombre"));
            opcion.addProperty("tipo", rs.getString("tipo"));
            opcion.addProperty("capacidad", rs.getString("capacidad"));
            respuesta.add(opcion);
        }
        
        return respuesta;
    }
    
    
    public JsonArray obtenerCategorias() throws SQLException {
        JsonArray respuesta = new JsonArray();
        JsonObject opcion = new JsonObject();

        String query = "SELECT id, nombre FROM categorias_equipo";
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();

        ResultSet rs = stmt.executeQuery(query); 
        
        while (rs.next()) {
            opcion = new JsonObject();
            opcion.addProperty("id", rs.getString("id"));
            opcion.addProperty("nombre", rs.getString("nombre"));
            respuesta.add(opcion);
        }
        
        return respuesta;
    }
    
    public void insertarAula(String clave, String nombreAula, String tipo, String capacidad) throws SQLException {
        String query = "INSERT INTO aulas (id, nombre, tipo, capacidad) VALUES ('"+clave+"', '"+nombreAula+"', '"+tipo+"', "+capacidad+")";
                
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();

        stmt.executeUpdate(query);
        
    }
    
    public void insertarEquipo(String nombreEquipo, String descripcionEquipo, String categoria) throws SQLException {
        String query = "INSERT INTO equipo (nombre, descripcion, id_categoria) VALUES ('"+nombreEquipo+"', '"+descripcionEquipo+"', "+categoria+")";
                
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();

        stmt.executeUpdate(query);
        
    }
    
    public void insertarCategoria(String nombreCategoria, String descripcionCategoria) throws SQLException {
        String query = "INSERT INTO categorias_equipo (nombre,descripcion) VALUES ('"+nombreCategoria+"', '"+descripcionCategoria+"')";
                
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();

        stmt.executeUpdate(query);
        
    }
    
    public JsonArray obtenerRegistroAula(String id) throws SQLException {
        JsonArray respuesta = new JsonArray();
        JsonObject opcion = new JsonObject();

        String query = "SELECT id, nombre, tipo, capacidad FROM aulas WHERE id = '"+ id+"'";
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();

        ResultSet rs = stmt.executeQuery(query); 
        
        while (rs.next()) {
            opcion = new JsonObject();
            opcion.addProperty("nombre", rs.getString("nombre"));
            opcion.addProperty("tipo", rs.getString("tipo"));
            opcion.addProperty("capacidad", rs.getString("capacidad"));
            respuesta.add(opcion);
        }
        
        return respuesta;
    }
    
    public void actualizarAula(String id, String clave, String nombreAula, String tipo, String capacidad) throws SQLException {
        String query = "UPDATE aulas set nombre = '" + nombreAula + "', tipo = '" + tipo + "', capacidad = '"+capacidad+"' WHERE id = '"+id+"'";
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();

        stmt.executeUpdate(query); 
        
    }
    
}
