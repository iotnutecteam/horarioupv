/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jtrevino.clazo.apirest.SQLConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author cristianlazo
 */
public class Grupos {

    public Grupos() {
    }
    
    public JsonArray obtenerGrupos() throws SQLException {
        JsonArray respuesta = new JsonArray();
        JsonObject opcion = new JsonObject();

        String query = "SELECT clv_plan FROM plan_estudios WHERE idcarrera = '" + SharedData.getCarrera() + "'";
        
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();
        Statement stmt2 = con.createStatement();

        ResultSet rs = stmt.executeQuery(query); 
        
        while (rs.next()) {
            ResultSet rs2 = stmt2.executeQuery("SELECT clv_grupo FROM grupos WHERE clv_plan = '" + rs.getString("clv_plan") + "'"); 
            while (rs2.next()) {
                String clave = rs2.getString("clv_grupo");
                opcion = new JsonObject();
                opcion.addProperty("clv_grupo", rs2.getString("clv_grupo"));
                respuesta.add(opcion);
            }
        }
        return respuesta;
    }
    
    
    public JsonArray obtenerHorario(String turno, String grupo) throws SQLException {
        JsonArray respuesta = new JsonArray();
        JsonObject opcion = new JsonObject();
        String query = "";

        if ("Matutino".equals(turno)) {
            query = "SELECT nombre_profesor, dia, hora, clv_materia FROM grupo_asignacion WHERE clv_grupo = '" +grupo+ "' AND hora <= 8 ORDER BY hora, dia";
        } else {
            query = "SELECT nombre_profesor, dia, hora, clv_materia FROM grupo_asignacion WHERE clv_grupo = '" +grupo+ "' AND hora >= 8 ORDER BY hora, dia";
        }
                        
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();
        Statement stmt2 = con.createStatement();

        ResultSet rs = stmt.executeQuery(query); 
        
        while (rs.next()) {
            opcion = new JsonObject();
            opcion.addProperty("nombre_profesor", rs.getString("nombre_profesor"));
            opcion.addProperty("dia", rs.getString("dia"));
            opcion.addProperty("hora", rs.getString("hora"));
            opcion.addProperty("clv_materia", rs.getString("clv_materia"));
            
            ResultSet rs2 = stmt2.executeQuery("SELECT nombre_materia FROM materias where clv_materia = '" + rs.getString("clv_materia") + "'"); 
            while (rs2.next()) {
                opcion.addProperty("nombre_materia", rs2.getString("nombre_materia"));
            }
            respuesta.add(opcion);
        }
        return respuesta;
    }
    
    public JsonArray obtenerPlanEstudios() throws SQLException {
        JsonArray respuesta = new JsonArray();
        JsonObject opcion = new JsonObject();
        String query = "SELECT clv_plan FROM plan_estudios WHERE idcarrera = '" + SharedData.getCarrera() + "'";
                        
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();

        ResultSet rs = stmt.executeQuery(query); 
        
        while (rs.next()) {
            opcion = new JsonObject();
            opcion.addProperty("clv_plan", rs.getString("clv_plan"));
            respuesta.add(opcion);
        }
        return respuesta;
    }
    
    
    public void insertarGrupo(String clave, String cuatrimestre, String planEstudio) throws SQLException {
        String query = "INSERT INTO grupos VALUES ('"+clave+"', '"+cuatrimestre+"', '"+planEstudio+"')";
        
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();

        stmt.executeUpdate(query);
        
    }
    
}
