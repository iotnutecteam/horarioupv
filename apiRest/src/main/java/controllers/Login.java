/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jtrevino.clazo.apirest.SQLConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author cristianlazo
 */
public class Login {
    
    private String usuario;
    private String password;

    public Login(String usuario, String password) {
        this.usuario = usuario;
        this.password = password;
    }
    
    public int checkLogin() throws SQLException {
        String query = "SELECT clv_usuario,pass_usuario,tipo_usuario,idcarrera FROM usuarios WHERE clv_usuario = '"+this.usuario+"' AND pass_usuario = PASSWORD('"+this.password+"')";
        JsonArray respuesta = new JsonArray();
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();

        ResultSet rs = stmt.executeQuery(query); 
        
        while (rs.next()) {
            JsonObject objeto = new JsonObject();
            String user = rs.getString("clv_usuario");
            String cont = rs.getString("pass_usuario");
            String tipo = rs.getString("tipo_usuario");
            String carrera = rs.getString("idcarrera");
            
            objeto.addProperty("clv_usuario", user);
            objeto.addProperty("pass_usuario", cont);
            objeto.addProperty("tipo_usuario", tipo);
            objeto.addProperty("idcarrera", carrera);
            
            if(tipo.equals("ADMI")){
                tipo = "Administrador";
            }else if (tipo.equals("DIRE")){
                tipo = "Director";
            }else{
                tipo = "Profesor";
            }
            
            SharedData.setTipoUsuario(tipo);
            SharedData.setClave(user);
            SharedData.setCarrera(carrera);
                    
            // Añadir objeto al la lista JSON
            respuesta.add(objeto);
        }

        return respuesta.size();
    }
   
    
}
