/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jtrevino.clazo.apirest.SQLConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author cristianlazo
 */
public class PlanEstudios {

    public PlanEstudios() {
    }
    
    public JsonArray obtenerPlanes() throws SQLException {
        JsonArray respuesta = new JsonArray();
        JsonObject opcion = new JsonObject();

        String query = "SELECT * FROM plan_estudios";
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();

        ResultSet rs = stmt.executeQuery(query); 
        
        while (rs.next()) {
            opcion = new JsonObject();
            opcion.addProperty("clv_plan", rs.getString("clv_plan"));
            opcion.addProperty("nombre_plan", rs.getString("nombre_plan"));
            opcion.addProperty("nivel", rs.getString("nivel"));
            opcion.addProperty("idcarrera", rs.getString("idcarrera"));
            respuesta.add(opcion);
        }
        
        return respuesta;
    }
    
    public void actualizarPlan(String oldClavePlan, String clavePlan, String nombrePlan, String nivelPlan, String carreraPlan) throws SQLException {
        
        String query = "UPDATE plan_estudios SET clv_plan = '"+clavePlan+"', nombre_plan = '"+nombrePlan+"', nivel = '"+nivelPlan+"', idcarrera = "+carreraPlan+" WHERE clv_plan = '"+oldClavePlan+"'";
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();

        stmt.executeUpdate(query); 
        
    }
    
    public void insertarPlan(String clavePlan, String nombrePlan, String nivelPlan, String carreraPlan) throws SQLException {
        String query = "INSERT INTO plan_estudios VALUES ('"+clavePlan+"', '"+nombrePlan+"', '"+nivelPlan+"', "+carreraPlan+")";
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();

        stmt.executeUpdate(query);
        
    }
    
}
