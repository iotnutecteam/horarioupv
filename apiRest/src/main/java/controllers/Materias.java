/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jtrevino.clazo.apirest.SQLConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author cristianlazo
 */
public class Materias {

    public Materias() {
    }
    
    public JsonArray obtenerPlanEstudios() throws SQLException {
        JsonArray respuesta = new JsonArray();
        JsonObject opcion = new JsonObject();

        String query = "SELECT clv_plan FROM plan_estudios WHERE idcarrera = '" + SharedData.getCarrera() +"'";
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();

        ResultSet rs = stmt.executeQuery(query); 
        
        while (rs.next()) {
            opcion = new JsonObject();
            opcion.addProperty("clv_plan", rs.getString("clv_plan"));
            respuesta.add(opcion);
        }
        
        return respuesta;
    }
    
    public JsonArray obtenerMaterias(String clv_plan) throws SQLException {
        JsonArray respuesta = new JsonArray();
        JsonObject opcion = new JsonObject();

        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();
        
         //El query se realiza de forma que el JSON regrese de forma ordenada por cuatrimestres y posiciones
        for (int i = 1; i <= 7; i++) {
            for (int j = 1; j <= 9; j++) {
                String query = "SELECT clv_materia,nombre_materia FROM materias WHERE clv_plan = '" + clv_plan + "' AND cuatrimestre = "+j+" AND posicion = "+i;
                ResultSet rs = stmt.executeQuery(query); 
                while (rs.next()) {
                    opcion = new JsonObject();
                    opcion.addProperty("clv_materia", rs.getString("clv_materia"));
                    opcion.addProperty("nombre_materia", rs.getString("nombre_materia"));
                    //opcion.addProperty("cuatrimestre", rs.getString("cuatrimestre"));
                    //opcion.addProperty("posicion", rs.getString("posicion"));
                    respuesta.add(opcion);
                }
            }
        }

        return respuesta;
    }
    
    public JsonArray obtenerDatosMateria(String clv_materia) throws SQLException {
        JsonArray respuesta = new JsonArray();
        JsonObject opcion = new JsonObject();

        String query = "SELECT clv_materia, nombre_materia, creditos, cuatrimestre, clv_plan, num_horas, tipo_materia FROM materias WHERE clv_materia = '"+clv_materia+"'";
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();

        ResultSet rs = stmt.executeQuery(query); 
        
        while (rs.next()) {
            opcion = new JsonObject();
            opcion.addProperty("clv_materia", rs.getString("clv_materia"));
            opcion.addProperty("nombre_materia", rs.getString("nombre_materia"));
            opcion.addProperty("creditos", rs.getString("creditos"));
            opcion.addProperty("cuatrimestre", rs.getString("cuatrimestre"));
            opcion.addProperty("clv_plan", rs.getString("clv_plan"));
            opcion.addProperty("num_horas", rs.getString("num_horas"));
            opcion.addProperty("tipo_materia", rs.getString("tipo_materia"));
            respuesta.add(opcion);
        }
        
        return respuesta;
    }
    
    public void actualizarMateria(String id, String clave, String nombreMateria, String creditos, String tipoMateria, String numeroHoras) throws SQLException {
        
        if (tipoMateria.equals("Tronco Común")) {
            tipoMateria = "TRC";
        } else if (tipoMateria.equals("Ciencias Básicas")) {
            tipoMateria = "CB";
        } else if (tipoMateria.equals("Especialidad")) {
            tipoMateria = "E";
        }
        
        String query = "UPDATE materias set clv_materia = '" + clave + "', nombre_materia = '" + nombreMateria + "', creditos = '" + creditos + "', tipo_materia = '" + tipoMateria + "', num_horas = '" + numeroHoras + "' WHERE clv_materia = '"+id+"'";
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();

        stmt.executeUpdate(query); 
        
    }
    
}

