/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jtrevino.clazo.apirest.SQLConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author cristianlazo
 */
public class PuntosConfianza {

    public PuntosConfianza() {
    }
    
    public JsonArray obtenerPlan() throws SQLException {
        JsonArray respuesta = new JsonArray();
        JsonObject opcion = new JsonObject();

        String query = "SELECT idcarrera FROM usuarios where clv_usuario = '" + SharedData.getClave() + "'";
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();
        Statement stmt2 = con.createStatement();

        ResultSet rs = stmt.executeQuery(query);  
        
        while (rs.next()) {
            ResultSet rs2 = stmt2.executeQuery("SELECT clv_plan FROM plan_estudios where idcarrera = '" + rs.getString("idcarrera") + "'");
            while (rs2.next()) {
                opcion = new JsonObject();
                opcion.addProperty("clv_plan", rs2.getString("clv_plan"));
                respuesta.add(opcion);
            }
            
        }
        
        return respuesta;
    }
    
    public JsonArray obtenerProfesores() throws SQLException {
        JsonArray respuesta = new JsonArray();
        JsonObject opcion = new JsonObject();

        String query = "SELECT idcarrera FROM usuarios where clv_usuario = '" + SharedData.getClave() + "'";
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();
        Statement stmt2 = con.createStatement();

        ResultSet rs = stmt.executeQuery(query);  
        
        while (rs.next()) {
            ResultSet rs2 = stmt2.executeQuery("SELECT nombre_usuario FROM usuarios where idcarrera = '" + rs.getString("idcarrera") + "' and tipo_usuario != 'ADMI'");
            while (rs2.next()) {
                opcion = new JsonObject();
                opcion.addProperty("nombre_usuario", rs2.getString("nombre_usuario"));
                respuesta.add(opcion);
            }
            
        }
        
        return respuesta;
    }
    
    public JsonArray obtenerPuntosMaterias(String profesor, String planEstudio) throws SQLException {
        JsonArray respuesta = new JsonArray();
        JsonObject opcion = new JsonObject();

        //Codigo para obtener la clave del profesor actual
        String clv_pp="";
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        String query = "SELECT clv_usuario FROM usuarios WHERE nombre_usuario = '" + profesor + "'";
        Statement stmt = con.createStatement();
        Statement stmt2 = con.createStatement();
        Statement stmt3 = con.createStatement();
        
        ResultSet rs = stmt.executeQuery(query);  
        
        
        while(rs.next()) {
            clv_pp = rs.getString("clv_usuario");
        }
       
        
        //El query se realiza de forma que el JSON regrese de forma ordenada por cuatrimestres y posiciones
        for (int i = 1; i <= 7; i++) {
            for (int j = 1; j <= 9; j++) {
                String query2 = "SELECT clv_materia,nombre_materia FROM materias WHERE clv_plan = '" + planEstudio + "' AND cuatrimestre = "+j+" AND posicion = "+i;
                ResultSet rs2 = stmt2.executeQuery(query2);
                while (rs2.next()) {
                    opcion = new JsonObject();
                    opcion.addProperty("clv_materia", rs2.getString("clv_materia"));
                    opcion.addProperty("nombre_materia", rs2.getString("nombre_materia"));
                    
                    ResultSet rs3 = stmt3.executeQuery("SELECT puntos_director FROM materia_usuario2 WHERE clv_materia = '" + rs2.getString("clv_materia") + "' and clv_plan = '" + planEstudio + "' and clv_usuario = '" + clv_pp + "'");
                    while (rs3.next()) {
                        opcion.addProperty("puntos", rs3.getString("puntos_director"));
                    }
                    respuesta.add(opcion);
                }
            }
        }
        return respuesta;
    }
    
    public JsonArray obtenerPuntosMateriasProf(String planEstudio) throws SQLException {
        JsonArray respuesta = new JsonArray();
        JsonObject opcion = new JsonObject();

        //Codigo para obtener la clave del profesor actual
        String clv_pp;
        clv_pp = SharedData.getClave();
        
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();
        Statement stmt2 = con.createStatement();
               
        
        //El query se realiza de forma que el JSON regrese de forma ordenada por cuatrimestres y posiciones
        for (int i = 1; i <= 7; i++) {
            for (int j = 1; j <= 9; j++) {
                String query = "SELECT clv_materia,nombre_materia FROM materias WHERE clv_plan = '" + planEstudio + "' AND cuatrimestre = "+j+" AND posicion = "+i;
                ResultSet rs = stmt.executeQuery(query);
                while (rs.next()) {
                    opcion = new JsonObject();
                    opcion.addProperty("clv_materia", rs.getString("clv_materia"));
                    opcion.addProperty("nombre_materia", rs.getString("nombre_materia"));
                    
                    ResultSet rs2 = stmt2.executeQuery("SELECT puntos_confianza FROM materia_usuario2 WHERE clv_materia = '" + rs.getString("clv_materia") + "' and clv_plan = '" + planEstudio + "' and clv_usuario = '" + clv_pp + "'");
                    while (rs2.next()) {
                        opcion.addProperty("puntos", rs2.getString("puntos_confianza"));
                    }
                    respuesta.add(opcion);
                }
            }
        }
        return respuesta;
    }
    
}
