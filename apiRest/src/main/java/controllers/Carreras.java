/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jtrevino.clazo.apirest.SQLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author cristianlazo
 */
public class Carreras {

    public Carreras() {
    }
        
    public JsonArray obtenerCarreras() throws SQLException {
        JsonArray respuesta = new JsonArray();
        JsonObject opcion = new JsonObject();

        String query = "SELECT * FROM carrera";
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();

        ResultSet rs = stmt.executeQuery(query); 
        
        while (rs.next()) {
            opcion = new JsonObject();
            opcion.addProperty("idcarrera", rs.getString("idcarrera"));
            opcion.addProperty("nombre_carrera", rs.getString("nombre_carrera"));
            respuesta.add(opcion);
        }
        
        return respuesta;
    }
    
    public void actualizarCarrer(String id, String nombreCarrera) throws SQLException {
        String query = "UPDATE carrera set nombre_carrera = '"+nombreCarrera+"' WHERE idcarrera = "+id;
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();

        stmt.executeUpdate(query); 
        
    }
    
    public void insertarCarrera(String nombre_carrera) throws SQLException {
        String query = "INSERT INTO carrera (nombre_carrera) VALUES ('"+nombre_carrera+"')";
        
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();

        stmt.executeUpdate(query);
        
    }
    
}
