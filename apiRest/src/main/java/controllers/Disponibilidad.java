/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jtrevino.clazo.apirest.SQLConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author cristianlazo
 */
public class Disponibilidad {

    public Disponibilidad() {
    }
    
    public JsonArray obtenerProfesores() throws SQLException {
        JsonArray respuesta = new JsonArray();
        JsonObject opcion = new JsonObject();
        
        String query = "";
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();
        String clv_usuario = SharedData.getClave();
        
        //Si ha iniciado sesión un profesor sólo puede modificar su disponibilidad y no la de los demás.
        if (SharedData.getTipoUsuario().equals("Profesor")) {    
            //Se obtiene el nombre del profesor por medio de su clave.
            query = "SELECT clv_usuario,nombre_usuario "
                    + "FROM usuarios "
                    + "WHERE clv_usuario = '" + clv_usuario + "'";
            //En caso de que sea un director o administrador puede configurar la dipsonibilidad de cualquier profesor de su carrera.
        } else {
            query = "SELECT clv_usuario,nombre_usuario FROM usuarios WHERE clv_usuario != 'admin' and idcarrera = (SELECT idcarrera FROM usuarios WHERE clv_usuario = '" + clv_usuario + "')";
        }

        ResultSet rs = stmt.executeQuery(query); 
        
        while (rs.next()) {
            opcion = new JsonObject();
            opcion.addProperty("clv_usuario", rs.getString("clv_usuario"));
            opcion.addProperty("nombre_usuario", rs.getString("nombre_usuario"));
            respuesta.add(opcion);
        }
        
        con.close();
        
        return respuesta;
    }
    
    public JsonArray obtenerDisponibilidad(String clv_usuario,String turno) throws SQLException {
        JsonArray respuesta = new JsonArray();
        JsonObject opcion = new JsonObject();

        String query = "";
        
        // Turno Matutino
        if("0".equals(turno)){
            //Se obtienen los datos de la base de datos para el usuario elegido.
            query = "SELECT d.dia, d.espacio_tiempo, d.clv_usuario "
                    + "FROM disponibilidad d JOIN usuarios u ON(d.clv_usuario = u.clv_usuario) "
                    + "WHERE u.clv_usuario = '" + clv_usuario + "'"
                    + "and espacio_tiempo <= '7'";
        }else{
            //Se obtienen los datos de la base de datos para el usuario elegido.
            query = "SELECT d.dia, d.espacio_tiempo, d.clv_usuario "
                    + "FROM disponibilidad d JOIN usuarios u ON(d.clv_usuario = u.clv_usuario) "
                    + "WHERE u.clv_usuario = '" + clv_usuario + "'"
                    + "and espacio_tiempo > '"+ turno +"'";
        }
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();

        ResultSet rs = stmt.executeQuery(query); 
        
        while (rs.next()) {
            opcion = new JsonObject();
            opcion.addProperty("dia", rs.getInt("dia"));
            opcion.addProperty("espacio_tiempo", rs.getInt("espacio_tiempo"));
            opcion.addProperty("clv_usuario", rs.getString("clv_usuario"));
            
            //jTable1.setValueAt(catedratico, rs.getInt("espacio_tiempo") - (turno+1), rs.getInt("dia") - 1);
            respuesta.add(opcion);
        }
        con.close();
        return respuesta;
    }
    
    public JsonArray actualizarDisponibilidad(String clv_usuario,String dia,String espacio_tiempo,String accion) throws SQLException {
        JsonArray respuesta = new JsonArray();
        JsonObject opcion = new JsonObject();

        String query = "";
        
        // Turno Matutino
        if("0".equals(accion)){
            // Borrar disponibilidad
            query = "DELETE FROM disponibilidad "
                    + "WHERE dia = '" + dia + "' and espacio_tiempo = '" +espacio_tiempo+ "'"
                    + " and clv_usuario = '" + clv_usuario + "'";
            
        }else{
            // Agregar nueva hora disponible
            query = "INSERT INTO disponibilidad(dia, espacio_tiempo, clv_usuario) "
                    + "VALUES('" + dia + "','" + espacio_tiempo + "','" + clv_usuario + "')";
        }
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();
        stmt.executeUpdate(query);
        
        opcion = new JsonObject();
        opcion.addProperty("exito", 1);
        respuesta.add(opcion);
        
        con.close();
        return respuesta;
    }
    
}
