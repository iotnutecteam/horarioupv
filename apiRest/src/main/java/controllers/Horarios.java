/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jtrevino.clazo.apirest.SQLConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author cristianlazo
 */
public class Horarios {
    private int y=0;
    private int hrs1 = 0;
    private int hrs2 = 0;
    private int hrs3 = 0;
    private int hrs4 = 0;
    private int hrs5 = 0;
    private int hrs6 = 0;
    
    private int chrs1 = 0;
    private int chrs2 = 0;
    private int chrs3 = 0;
    private int chrs4 = 0;
    private int chrs5 = 0;
    private int chrs6 = 0;
    
    private int hrsmax;
    
    public Horarios() {
    }
    
    public JsonArray actualizarHorarioGrupo(String clv_grupo,String clv_materia,String clv_profesor,String nombre_profesor,String dia,String hora) throws SQLException {
        JsonArray respuesta = new JsonArray();
        JsonObject opcion = new JsonObject();
        
        String query = "";
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();
        opcion = new JsonObject();
        
        try {
            ///validando integridad
            ValidarIntegridad(clv_grupo,clv_materia,clv_profesor,nombre_profesor,dia, hora);
            
            System.out.println("VALIDACION: "+this.y);
            //y = 0;
            
            if (y == 1) {
                try {
                    query = "DELETE FROM grupo_asignacion WHERE dia = '" + dia + "' AND hora = '" + hora + "' AND nombre_profesor = '" + nombre_profesor + "' AND clv_materia = '" + clv_materia + "'";
                    stmt.executeUpdate(query);
                    opcion.addProperty("mensaje", "0");
                } catch (Exception ex) {
                    System.out.println("ex-1" + ex);
                }
                
                //Actualizar();

            } else if (y == 0) {
                try {
                    //hacer insert
                    //Actualizar();
                    
                    query = "INSERT INTO grupo_asignacion values ('" + clv_grupo + "','" + clv_materia + "','" + dia + "','" + hora + "','" + nombre_profesor + "')";
                    stmt.executeUpdate(query);
                    
                    opcion.addProperty("mensaje", "1");
                } catch (Exception ex) {
                    System.out.println("ex-2" + ex);
                }
            } else if (y == -1) {
                opcion.addProperty("mensaje", "El grupo ya tiene esa hora asignada.");
            }else if( y == -4){
                opcion.addProperty("mensaje","El profesor no puede dar clases a esa hora.");
            } else if (y == -5) {
                opcion.addProperty("mensaje", "No se puede dar una materia mas de dos horas al dia.");
            } else if (y == -6) {
                opcion.addProperty("mensaje", "Si elige 2 horas de materia en un mismo dia, tienen que ser seguidas.");
            } else if (y == -7) {
                opcion.addProperty("mensaje", "El profesor ya tiene esa hora asignada.");
            } else if (y == -8) {
                opcion.addProperty("mensaje", "El profesor ya esta dando otra materia.");
            }else if( y == -20){
                opcion.addProperty("mensaje", "Ya hay otro profesor dando esa materia: "+clv_materia);
            }else if( y == -30){
                opcion.addProperty("mensaje", "Ha superado el limite de horas de la materia:" + clv_materia);
            }else if( y==-3){
                opcion.addProperty("mensaje", "No puede superar el limite de horas del profesor.");
            }
            
        } catch (Exception ex) {
            System.out.println("ex-3" + ex);
        }
        
        
            
        respuesta.add(opcion);
        
        
        con.close();
        
        return respuesta;
    }
    
    public JsonArray obtenerHorasSemana(String clv_profesor,String nombre_profesor) throws SQLException {
        JsonArray respuesta = new JsonArray();
        JsonObject opcion = new JsonObject();
        
        String query = "";
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();
        
        this.hrs1 = 0;
        this.hrs2 = 0;
        this.hrs3 = 0;
        this.hrs4 = 0;
        this.hrs5 = 0;
        this.hrs6 = 0;
        this.chrs1 = 0;
        this.chrs2 = 0;
        this.chrs3 = 0;
        this.chrs4 = 0;
        this.chrs5 = 0;
        this.chrs6 = 0;
        
        //
        try {
            query = "SELECT * FROM disponibilidad where clv_usuario = '" + clv_profesor + "'";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                int dia = rs.getInt("dia");
                if (dia == 1) {
                    this.hrs1++;
                }
                if (dia == 2) {
                    this.hrs2++;
                }
                if (dia == 3) {
                    this.hrs3++;
                }
                if (dia == 4) {
                    this.hrs4++;
                }
                if (dia == 5) {
                    this.hrs5++;
                }
                if (dia == 6) {
                    this.hrs6++;
                }
            }
            con.close();
        } catch (Exception ex) {
            System.out.println("ex-28" + ex);
        }
        //hrsProfe.setText("Horas máximas:     " + this.hrs1 + "   " + this.hrs2 + "    " + this.hrs3 + "   " + this.hrs4 + "   " + this.hrs5 + "   " + this.hrs6 + "   ");
        actualizarhrs(nombre_profesor);
        
        
        
        opcion = new JsonObject();
        opcion.addProperty("hrs1", hrs1);
        opcion.addProperty("hrs2", hrs2);
        opcion.addProperty("hrs3", hrs3);
        opcion.addProperty("hrs4", hrs4);
        opcion.addProperty("hrs5", hrs5);
        opcion.addProperty("hrs6", hrs6);
        
        opcion.addProperty("res1", this.hrs1 - this.chrs1);
        opcion.addProperty("res2", this.hrs2 - this.chrs2);
        opcion.addProperty("res3", this.hrs3 - this.chrs3);
        opcion.addProperty("res4", this.hrs4 - this.chrs4);
        opcion.addProperty("res5", this.hrs5 - this.chrs5);
        opcion.addProperty("res6", this.hrs6 - this.chrs6);
        
        //hrsProfe1.setText("Horas restantes:     " + (this.hrs1 - this.chrs1) + "   " + (this.hrs2 - this.chrs2) + "    " + (this.hrs3 - this.chrs3) + "   " + (this.hrs4 - this.chrs4) + "   " + (this.hrs5 - this.chrs5) + "   " + (this.hrs6 - this.chrs6) + "   ");


        respuesta.add(opcion);
        
        
        con.close();
        
        return respuesta;
    }
    
    public JsonArray obtenerHorarioGrupo(String clv_grupo,String turno) throws SQLException {
        JsonArray respuesta = new JsonArray();
        JsonObject opcion = new JsonObject();
        
        String query = "";
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();
        
        // Si se selecciona turno matutino
        if("0".equals(turno)){
            query = "SELECT asig.clv_materia,asig.dia,asig.hora,asig.nombre_profesor,mat.nombre_materia,mat.num_horas FROM `grupo_asignacion` AS asig\n" +
            "INNER JOIN materias AS mat ON mat.clv_materia = asig.clv_materia\n" +
            "WHERE asig.clv_grupo ='"+clv_grupo+"' AND asig.hora <= 7";
        }else{
            query = "SELECT asig.clv_materia,asig.dia,asig.hora,asig.nombre_profesor,mat.nombre_materia,mat.num_horas FROM `grupo_asignacion` AS asig\n" +
            "INNER JOIN materias AS mat ON mat.clv_materia = asig.clv_materia\n" +
            "WHERE asig.clv_grupo ='"+clv_grupo+"' AND asig.hora > 7";
        }
        
        ResultSet rs = stmt.executeQuery(query);
        
        while (rs.next()) {
            opcion = new JsonObject();
            opcion.addProperty("clv_materia", rs.getString("clv_materia"));
            opcion.addProperty("dia", rs.getString("dia"));
            opcion.addProperty("hora", rs.getInt("hora"));
            opcion.addProperty("nombre_profesor", rs.getString("nombre_profesor"));
            opcion.addProperty("nombre_materia", rs.getString("nombre_materia"));
            opcion.addProperty("num_horas", rs.getString("num_horas"));
            
            
            respuesta.add(opcion);
        }
        
        con.close();
        
        return respuesta;
    }
    
    public JsonArray obtenerMateriasGrupo(String clv_grupo) throws SQLException {
        JsonArray respuesta = new JsonArray();
        JsonObject opcion = new JsonObject();
        
        String query = "";
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();
        
        query = "SELECT mat.clv_materia,mat.nombre_materia,mat.clv_plan,mat.cuatrimestre,mat.num_horas FROM `materias` AS mat\n" +
        "INNER JOIN grupos ON grupos.clv_plan = mat.clv_plan AND grupos.cuatrimestre = mat.cuatrimestre\n"+
        "WHERE grupos.clv_grupo = '"+clv_grupo+"'";
        
        ResultSet rs = stmt.executeQuery(query);
        
        while (rs.next()) {
            opcion = new JsonObject();
            opcion.addProperty("clv_materia", rs.getString("clv_materia"));
            opcion.addProperty("nombre_materia", rs.getString("nombre_materia"));
            opcion.addProperty("clv_plan", rs.getString("clv_plan"));
            opcion.addProperty("cuatrimestre", rs.getString("cuatrimestre"));
            opcion.addProperty("num_horas", rs.getString("num_horas"));
            
            
            respuesta.add(opcion);
        }
        
        con.close();
        
        return respuesta;
    }
    
    public JsonArray obtenerDisponibilidad(String clv_usuario,String turno) throws SQLException {
        JsonArray respuesta = new JsonArray();
        JsonObject opcion = new JsonObject();

        String query = "";
        
        // Turno Matutino
        if("0".equals(turno)){
            //Se obtienen los datos de la base de datos para el usuario elegido.
            query = "SELECT d.dia, d.espacio_tiempo, d.clv_usuario "
                    + "FROM disponibilidad d JOIN usuarios u ON(d.clv_usuario = u.clv_usuario) "
                    + "WHERE u.clv_usuario = '" + clv_usuario + "'"
                    + "and espacio_tiempo <= '7'";
        }else{
            //Se obtienen los datos de la base de datos para el usuario elegido.
            query = "SELECT d.dia, d.espacio_tiempo, d.clv_usuario "
                    + "FROM disponibilidad d JOIN usuarios u ON(d.clv_usuario = u.clv_usuario) "
                    + "WHERE u.clv_usuario = '" + clv_usuario + "'"
                    + "and espacio_tiempo > '"+ turno +"'";
        }
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();

        ResultSet rs = stmt.executeQuery(query); 
        
        while (rs.next()) {
            opcion = new JsonObject();
            opcion.addProperty("dia", rs.getInt("dia"));
            opcion.addProperty("espacio_tiempo", rs.getInt("espacio_tiempo"));
            opcion.addProperty("clv_usuario", rs.getString("clv_usuario"));
            
            //jTable1.setValueAt(catedratico, rs.getInt("espacio_tiempo") - (turno+1), rs.getInt("dia") - 1);
            respuesta.add(opcion);
        }
        con.close();
        return respuesta;
    }
    
    public JsonArray actualizarDisponibilidad(String clv_usuario,String dia,String espacio_tiempo,String accion) throws SQLException {
        JsonArray respuesta = new JsonArray();
        JsonObject opcion = new JsonObject();

        String query = "";
        
        // Turno Matutino
        if("0".equals(accion)){
            // Borrar disponibilidad
            query = "DELETE FROM disponibilidad "
                    + "WHERE dia = '" + dia + "' and espacio_tiempo = '" +espacio_tiempo+ "'"
                    + " and clv_usuario = '" + clv_usuario + "'";
            
        }else{
            // Agregar nueva hora disponible
            query = "INSERT INTO disponibilidad(dia, espacio_tiempo, clv_usuario) "
                    + "VALUES('" + dia + "','" + espacio_tiempo + "','" + clv_usuario + "')";
        }
        Connection con;
        SQLConnection sql = new SQLConnection();
        con = sql.Connection();
        
        Statement stmt = con.createStatement();
        stmt.executeUpdate(query);
        
        opcion = new JsonObject();
        opcion.addProperty("exito", 1);
        respuesta.add(opcion);
        
        con.close();
        return respuesta;
    }
    
    void ValidarIntegridad(String clv_grupo,String clv_materia,String clv_profesor,String nombre_profesor,String dia, String hora) throws Exception {
        //Actualizar();
        this.y = 0;
        int diahr = 0;
        int r = 0;
        int r2 = 0;
        int r3 = 0;
        
        int fila = Integer.parseInt(hora);
        int columna = Integer.parseInt(dia);
        //codigo que valida si ya hay dos horas puestas en la materia en un dia
        try {
            Connection con;
            SQLConnection sql = new SQLConnection();
            con = sql.Connection();

            Statement stmt = con.createStatement();
            String query = "SELECT * FROM grupo_asignacion WHERE clv_materia = '" + clv_materia + "' AND dia = '" + columna + "'";
            
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                r2++;
            }

            if (r2 == 2) {
                y = -5;
                //JOptionPane.showMessageDialog(null, "No se puede dar una materia mas de dos horas al dia.");
            }
            //codigo para validar que si hay 2 horas de una materia, que estas sean contiguas
            //codigo que valida si ya hay dos horas puestas en la materia en un dia

            query = "SELECT * FROM grupo_asignacion WHERE clv_materia = '" + clv_materia + "' AND dia = '" + columna + "'";
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                int horal = rs.getInt("hora");
                //int hora = rs.getInt("hora");                
                
                if (horal == (fila - 1) || horal == (fila + 1)) {

                } else {
                    y = -6;
                }
            }

            //codigo que valida si el profesor puede dar clase en esa hora
            r = 0;

            query = "SELECT * FROM disponibilidad WHERE espacio_tiempo = '" + fila + "' AND dia = '" + columna + "' AND clv_usuario = '" + clv_profesor + "'";
            rs = stmt.executeQuery(query);
            
            while (rs.next()) {
                r++;
            }

            if (r == 0) {
                y = -4;
                //JOptionPane.showMessageDialog(null, r + " El profesor no puede dar clases a esa hora. dia: " + columna + " hora: " + fila);
            }
            ////seccion para ver que no se superen las horas de un profesor

            query = "SELECT * FROM grupo_asignacion WHERE nombre_profesor = '" + nombre_profesor + "' AND dia = '" + columna + "'";
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                diahr++;
            }

            if (columna == 1) {
                if (diahr >= this.hrs1) {
                    y = -3;
                }
            }
            if (columna == 2) {
                if (diahr >= this.hrs2) {
                    y = -3;
                }
            }
            if (columna == 3) {
                if (diahr >= this.hrs3) {
                    y = -3;
                }
            }
            if (columna == 4) {
                if (diahr >= this.hrs4) {
                    y = -3;
                }
            }
            if (columna == 5) {
                if (diahr >= this.hrs5) {
                    y = -3;
                }
            }
            if (columna == 6) {
                if (diahr >= this.hrs6) {
                    y = -3;
                }
            }
            if (y == -3) {
                //JOptionPane.showMessageDialog(null, "No puede superar el limite de horas del profesor.");
            }
            //seccion para checar que los profesores no se repitan en las mismas horas

            query = "SELECT * FROM grupo_asignacion WHERE dia = '" + (columna) + "' AND hora ='" + (fila) + "' AND nombre_profesor = '" + nombre_profesor + "'";
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                this.y = -7;
            }

            int h = 0, y = 0;
            /////
            //En este apartado se checa que un grupo no tenga horas repetidas

            query = "SELECT * FROM grupo_asignacion WHERE dia = '" + (columna) + "' AND hora ='" + (fila) + "' AND clv_grupo = '" + clv_grupo + "'";

            rs = stmt.executeQuery(query);
            while (rs.next()) {
                if (clv_grupo.equals(rs.getString("clv_grupo"))) {
                    this.y = -1;
                }
            }

            ////
            ///Validacion para que un profesor no de dos materias distintas en un mismo grupo
            String clmat = "";

            query = "SELECT * FROM grupo_asignacion WHERE clv_grupo = '" + clv_grupo + "' AND nombre_profesor ='" + nombre_profesor + "'";
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                clmat = rs.getString("clv_materia");
                if (!clv_materia.equals(clmat)) {
                    this.y = -8;
                }
            }

            ///
            ///Validacion que checa que una materia impartida en un mismo grupo no se de con distintos profes, puede haber otro grupo con la misma materia y un unico profesor
            int l = 0;

            query = "SELECT * FROM grupo_asignacion WHERE clv_grupo = '" + clv_grupo + "' AND clv_materia ='" + clv_materia + "'";
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                h++;
                if (!nombre_profesor.equals(rs.getString("nombre_profesor"))) {
                    l = 1;
                    this.y = -2;
                }
            }
            //conn.desconectar();

            if (l == 1) {
                this.y = -20;
                
                //JOptionPane.showMessageDialog(null, "Ya hay otro profesor dando esa materia.");
            }
            ////
            /////caso en que un profe, materia y hora sean los mismos, para quitarlo de la base de datos

            query = "SELECT * FROM grupo_asignacion WHERE dia = '" + (columna) + "' AND hora ='" + (fila) + "' AND nombre_profesor = '" + nombre_profesor + "'";
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                h++;
                if (clv_materia.equals(rs.getString("clv_materia"))) {
                    this.y = 1;
                }
            }

            //
            if (h > 0) {//si es mayor a 1 significa que ya hay un resgistro con esa hora, dia y el mismo profesor, entonces esta ocupado en esa hora
                if (this.y == 1) {
                    //do nothing
                    //JOptionPane.showMessageDialog(null, "Eliminando profesor de hora...");
                } else if (this.y >= 0) {
                    // JOptionPane.showMessageDialog(null, "El profesor en cuestion ya esta ocupado a esa hora. Dia:" + (columna) + " Hora:" + (fila));
                    //throw new Exception();
                }

            }

            //se evalua que una materia no supere el maximo de horas en el grupo
            int c = 0;
            
            /*
            for (int j = 0; j < this.VProfesores.size(); j++) {
                if (this.VMaterias.get(j).equals(claveMateria)) {
                    c++;

                }
            }
            if (hrsmax <= c && this.y == 0) {
                this.y = -30;
                //JOptionPane.showMessageDialog(null, "Ha superado el limite de horas de la materia:" + claveMateria);
                throw new Exception();
            }
            */

            //
            //Actualizar();
        } catch (Exception e) {
            System.out.println("ex-nose" + e);
        }
    }
    
    void actualizarhrs(String nombre_profesor) {
        this.chrs1 = 0;
        this.chrs2 = 0;
        this.chrs3 = 0;
        this.chrs4 = 0;
        this.chrs5 = 0;
        this.chrs6 = 0;
        try {
            String query = "";
            Connection con;
            SQLConnection sql = new SQLConnection();
            con = sql.Connection();

            Statement stmt = con.createStatement();
            
            query = "SELECT * FROM grupo_asignacion WHERE nombre_profesor = '" + nombre_profesor + "'";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                int dia = rs.getInt("dia");
                if (dia == 1) {
                    this.chrs1++;
                }
                if (dia == 2) {
                    this.chrs2++;
                }
                if (dia == 3) {
                    this.chrs3++;
                }
                if (dia == 4) {
                    this.chrs4++;
                }
                if (dia == 5) {
                    this.chrs5++;
                }
                if (dia == 6) {
                    this.chrs6++;
                }
            }
            con.close();
            
        } catch (Exception ex) {
            System.out.println("ex-22" + ex);
        }
        //hrsProfe1.setText("Horas restantes:     " + (this.hrs1 - this.chrs1) + "   " + (this.hrs2 - this.chrs2) + "    " + (this.hrs3 - this.chrs3) + "   " + (this.hrs4 - this.chrs4) + "   " + (this.hrs5 - this.chrs5) + "   " + (this.hrs6 - this.chrs6) + "   ");
    }
    
    public JsonArray obtenerIMRGrupo(String clv_grupo) throws SQLException {
        JsonArray respuesta = new JsonArray();
        JsonObject opcion = new JsonObject();
        
        float calIMR = actualizarIMR(clv_grupo);
        
        opcion = new JsonObject();
        opcion.addProperty("IMR", calIMR);
        respuesta.add(opcion);
        
        return respuesta;
    }
    
    float actualizarIMR(String clv_grupo) {
        int k = 0;
        ///codiho que va a obtener los nombres de los profesores para poder realizar la probabilidad de reprobacion
        String[] nombres = new String[20];//se guardan los nombres de los profesores, en teoria son 7 maximo por grupo
        int tam2 = 0;
        try {
            
            String query = "";
            Connection con;
            SQLConnection sql = new SQLConnection();
            con = sql.Connection();

            Statement stmt = con.createStatement();
            query = "SELECT * FROM grupo_asignacion WHERE clv_grupo = '" + clv_grupo + "'";
            ResultSet rs = stmt.executeQuery(query);
            
            while (rs.next()) {
                k = 0;
                String prof = rs.getString("nombre_profesor");
                for (int i = 0; i < nombres.length; i++) {
                    if (nombres[i] != null) {
                        if (nombres[i].equals(prof)) {
                            k = 1;
                        }
                    }
                }
                if (k == 0) {
                    nombres[tam2] = prof;
                    tam2++;
                }
            }
            rs.close();
            con.close();
        } catch (Exception ex) {
            System.out.println("ex-25" + ex);
        }

        int tam = 0;
        String[] claves = new String[20];
        int[] IMR = new int[20];
        ////
        for (int i = 0; i < nombres.length; i++) {
            if (nombres[i] != null) {
                try {
                    Connection con;
                    SQLConnection sql = new SQLConnection();
                    con = sql.Connection();

                    Statement stmt = con.createStatement();
                    
                    String query = "SELECT * FROM usuarios WHERE nombre_usuario = '" + nombres[i] + "'";
                    ResultSet rs = stmt.executeQuery(query);
                    
                    while (rs.next()) {
                        k = 0;
                        claves[i] = rs.getString("clv_usuario");
                        IMR[i] = rs.getInt("IMR");
                        tam++;
                    }
                    rs.close();
                    con.close();
                } catch (Exception ex) {
                    System.out.println("ex-26" + ex);
                }
            }
        }
        int prom = 0;

        for (int i = 0; i < tam; i++) {
            if (IMR[i] != 0) {
                prom = prom + IMR[i];
            }
        }
        if (tam != 0) {
            //probabilidadRep2.setText("Probabilidad de reprobación de grupo: " + prom / tam);
            return prom / tam;
        }
        
        return 0;
    }
    
}
