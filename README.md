# HorarioUPV

Repositorio código de fuente REST API de la implementación en JavaSwing.

# Requerimientos de software
- Entorno Linux
- Netbeans IDE.
- Instalación de Java 8 (1.8)
- MySQL versión 5.7 + herramienta para correr comandos de mysql en terminal.

# Instrucciones de instalación
1. Importar la base de datos(_horarios.sql_) que se encuentra en el directorio raíz de este proyecto(si ya tienen una BD del proyecto FX montada es importante eliminarla para evitar problemas de compatibilidad).
	- La importación puede hacerse desde la terminal de linux con el siguiente comando (requiere de la cuenta sudo de MySQL).
		1. Crear la base de datos con el nombre "_horarios_" para importar el contenido (omitir si ya ha sido creada con anticipación).
		
			```
			$ mysql -u root -p
			```
			
			```
			$ CREATE DATABASE horarios
			```
			
			```
			$ exit
			```
			
		2. En la terminal bajar al directorio(```cd```) del proyecto e importar contenido a la base de datos.
		
		
			```$ mysql -u root -p horarios < horarios.sql```
		
	- Como alternativa la importación también puede realizarse desde __PhpMyAdmin__ si lo tienen instalado (requiere de la cuenta sudo de MySQL).

2. Abrir la terminal de linux y conectarse a MySQL con la cuenta de súper usuario (presionar telca Enter e introducir contrseña de súper usuario MySQL).
	
	```
	$ mysql -u root -p
	```
	
	```
	$ CREATE USER 'horariosupv'@'localhost' IDENTIFIED BY 'by6V7#ZJ';
	```
	
	```
	$ GRANT ALL PRIVILEGES ON horarios.* TO 'horariosupv'@'localhost';
	```
	
	```
	$ FLUSH PRIVILEGES;
	```
	
	```
	$ exit
	```
	
	Como alternativa, los comandos anteriores(_CREATE USER_, _GRANT ALL_ Y _FLUSH PRIVILEGES_) también pueden ejecutarse en la ventana de consultas en la interfaz de __PhpMyAdmin__.

2. Importar(_abrir_) el proyecto en Netbeans IDE.

3. Compilar el proyecto. Comenzará el proceso de instalación de las dependencias Maven (_esperar..._).

4. Correr el proyecto.

5. Abrir una ventana en el navegador y acceder a la siguiente dirección:
	
	http://localhost:4567/
